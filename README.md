### Exjemplo Login PHP + bootstrap + mailer
- Ejemplo login/register con bootstrap + php
- Formulario con validacion de campos. verificacion de estado de usuario.
- Envio de mail de registración 
- Mail con datos para activar la cuenta

### Para el envío de correo leer link en el repositorio Clase03P sobre seguridad en cuenta de Gmail
### Deberan crear un archivo en la carpeta Data que contenga la configuración para el envio del correo con la siguiente estructura:

    - Sitio web | usuario de correo desde donde sale el mail | token

### Ejemplo 

    - http://localhost:8080/Clase0xx/activar.php|mail@gmail.com|token

### Tareas sugeridas

    - Crear una nueva rama y a partir de ella 
    - Mejorar la estructura del repositorio en cuanto a los archivos que se encuentran sueltos


